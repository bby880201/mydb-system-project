//
//  RelAlgExpr.cc
//  A7
//
//  Created by Boyang Bai on 4/27/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#include "RelAlgExpr.h"
#include "ScanJoin.h"
#include "SortMergeJoin.h"
#include <string>

RelAlgExpr :: RelAlgExpr (map <string, MyDB_TableReaderWriterPtr> loadedTables, SFWQuery query, MyDB_BufferManagerPtr bufMgr, MyDB_CatalogPtr cat) {
    
    myCatFile = cat;
    inputQuery = query;
    myMgr = bufMgr;
    init(loadedTables);
}

RelAlgExpr :: ~RelAlgExpr () {
    for (auto temp : tempTables) {
        auto name = temp->getTable()->getStorageLoc().c_str();
        remove(name);
    }
    remove("temp_groupings.bin");
}

void RelAlgExpr :: run () {
    clock_t begin = clock();
    checkJoin();
    finalSelAgg();
    clock_t end = clock();
    int elapsed_secs = int(end - begin) / CLOCKS_PER_SEC;
    cout << to_string(elapsed_secs) << "s\n";
}

void RelAlgExpr :: init(map <string, MyDB_TableReaderWriterPtr> loadedTables) {
    set<string> tableConflictCheck;
    for (int i = 0; i < inputQuery.tablesToProcess.size(); i++){
        pair<string, string> &tableName = inputQuery.tablesToProcess[i];
        if (tableConflictCheck.count(tableName.first) == 0) {
            tableConflictCheck.insert(tableName.first);
            allTables[tableName.second] = renameTable(loadedTables[tableName.first], tableName);
        } else {
            auto temp = loadedTables[tableName.first];
            tableName.first = tableName.first+"_1";
            tableConflictCheck.insert(tableName.first);
            allTables[tableName.second] = renameTable(temp, tableName);
            //            cout << inputQuery.tablesToProcess[i].first << " AS " << inputQuery.tablesToProcess[i].second<<endl;
        }
        joinTableMapping[tableName.first] = tableName.second;
        joinTableMapping[tableName.second] = tableName.second;
    }
    
    for (auto expr:inputQuery.valuesToSelect) {
        auto neededTables = expr->tableToJoin();
        if (neededTables.size() == 1) {
            string tbl1 = *(neededTables.begin());
            auto ids = expr->getAtomicIdentifiers();
            tableAttsMap[tbl1].insert(tableAttsMap[tbl1].begin(), ids.begin(), ids.end());
        } else if (neededTables.size() > 1){
            string key;
            for (auto tblName: neededTables) {
                key += "+" + tblName;
            }
            multiTblProjec[key] = expr;
            for (auto att: expr->getAtomicIdentifiers()){
                tableAttsMap[att.getTable()].push_back(att);
            }
        }
    }
    
    map<pair<string,string>, vector<ExprTreePtr>> preds;
    for (auto expr:inputQuery.allDisjunctions) {
        auto neededTables = expr->tableToJoin();
        if (neededTables.size()>1) {
            string tbl1 = *(neededTables.begin());
            string tbl2 = *next(neededTables.begin(),1);
            if (tbl1 < tbl2) {
                auto key = make_pair(tbl1, tbl2);
                preds[key].push_back(expr);
            } else {
                auto key = make_pair(tbl2, tbl1);
                preds[key].push_back(expr);
            }
            auto atts = expr->getAtomicIdentifiers();
            for (auto att:atts) {
                tableAttsMap[att.getTable()].push_back(att);
                
            }
        } else if (neededTables.size() == 1) {
            auto tbl = neededTables.begin();
            tableSelectionMap[*tbl].push_back(expr);
        }
    }
    joinPreds = sortJoinPred(preds);
    //
    //    for (auto expr:inputQuery.groupingClauses) {
    //        auto atts = expr->getAtomicIdentifiers();
    //        for (auto att:atts) {
    //            tableAttsMap[att.getTable()].push_back(att);
    //        }
    //    }
    //    for (auto tblAtt: tableAttsMap) {
    //        for (auto att: tblAtt.second) {
    //            cout <<"att: " << att.toString() << " in table: " << tblAtt.first<<endl;
    //        }
    //    }
    //    for (auto tblAtt: joinPreds) {
    //        for (auto tmp: tblAtt.second) {
    //            cout << "join tables: " << tblAtt.first.first<< " and "<<tblAtt.first.second << "\njoin predicate: "<< tmp->toString() <<endl;
    //        }
    //    }
    //    for (auto tblAtt: tableSelectionMap) {
    //        for (auto att: tblAtt.second) {
    //            cout <<"selection predicate: " << att->toString() << " in table: " << tblAtt.first<<endl;
    //        }
    //    }
    
}

void RelAlgExpr :: checkJoin() {
    if (joinPreds.size() == 0){
        return;
    }
    
    cout <<"running join!"<<endl;
    auto tblAtt = joinPreds.begin();
    while (tblAtt != joinPreds.end()) {
        cout <<"joining table: "<< tblAtt->first.first << " and " << tblAtt->first.second <<endl;
        //        joinPreds.erase(tblAtt.first);
        MyDB_TableReaderWriterPtr leftTbl,rightTbl;
        auto tblPair = tblAtt->first;
        
        for (auto tblName:inputQuery.tablesToProcess) {
            if (tblName.second == tblPair.first) {
                leftTbl = allTables[findTableName(tblName.first)];
            } else if (tblName.second == tblPair.second) {
                rightTbl = allTables[findTableName(tblName.first)];
            }
        }
        
        //        cout <<"first table: "<< leftTbl->getTable()->getName() <<endl;
        //        cout <<"second table: "<< rightTbl->getTable()->getName() <<endl;
        //
        //        cout <<"start projection!"<<endl;
        vector<Identifier> projection;
        
        for (auto expr:tblAtt->second) {
            for (auto att: expr->getAtomicIdentifiers()) {
                auto iter = tableAttsMap[att.getTable()].begin();
                while (iter != tableAttsMap[att.getTable()].end()) {
                    if (iter->getTable() == att.getTable() && iter->getAtt() == att.getAtt()) {
                        tableAttsMap[att.getTable()].erase(iter);
                        break;
                    }
                    iter++;
                }
            }
        }
        
        for (auto att:tableAttsMap[tblPair.first]) {
            projection.push_back(att);
        }
        for (auto att:tableAttsMap[tblPair.second]) {
            projection.push_back(att);
        }
        
        for (auto att:leftTbl->getTable()->getSchema()->getAtts()) {
            string tbl = att.first.substr(0,att.first.find("_"));
            for(auto inAtt: tableAttsMap[tbl]) {
                if ((inAtt.getTable()+"_"+inAtt.getAtt()) == att.first) {
                    projection.push_back(inAtt);
                }
            }
        }
        
        for (auto att:rightTbl->getTable()->getSchema()->getAtts()) {
            string tbl = att.first.substr(0,att.first.find("_"));
            for(auto inAtt: tableAttsMap[tbl]) {
                if ((inAtt.getTable()+"_"+inAtt.getAtt()) == att.first) {
                    projection.push_back(inAtt);
                }
            }
        }
        
        //        for (auto expr: inputQuery.valuesToSelect) {
        //            auto tables = expr->tableToJoin();
        //            if (tables.size() == 1) {
        //                auto table = *tables.begin();
        //                if (table == tblPair.first || table == tblPair.second) {
        //                    projection.push_back(expr);
        //                }
        //            } else if (tables.size() == 2) {
        //                string tbl1 = *(tables.begin());
        //                string tbl2 = *next(tables.begin(),1);
        //                if ((tbl1 == tblPair.first && tbl2 == tblPair.second) || (tbl1 == tblPair.second && tbl2 == tblPair.first)) {
        //                    projection.push_back(expr);
        //                }
        //            }
        //            for (auto att: expr->getAtomicIdentifiers()) {
        //                if (att.getTable() == tblPair.first || att.getTable() == tblPair.second) {
        //                    projection.push_back(att);
        //                } else if (joinTableMapping[att.getTable()] != att.getTable()) {
        //                    projection.push_back(att);
        //                }
        //            }
        //        }
        //        for (auto pro:projection) {
        //            cout <<"projection: "<< pro.toString() <<endl;
        //        }
        
        //        for (auto iter: tableAttsMap) {
        //            cout << "table: " << iter.first << " now has following atts: "<< endl;
        //            for (auto att: iter.second) {
        //                cout << att.toString() << "\t";
        //            }
        //            cout << "\n";
        //        }
        
        //        cout <<"start making predicates!" <<endl;
        
        string leftPred = getPredicateString(tableSelectionMap[tblPair.first]);
        cout << "left predicate: "<< leftPred <<endl;
        tableSelectionMap.erase(tblPair.first);
        
        string rightPred = getPredicateString(tableSelectionMap[tblPair.second]);
        cout << "right predicate: "<<rightPred <<endl;
        tableSelectionMap.erase(tblPair.second);
        
        //        cout <<"start making output schema!" <<endl;
        set<string> joinProj;
        vector<string> proj;
        MyDB_SchemaPtr mySchemaOut = make_shared <MyDB_Schema> ();
        for (auto att: projection) {
            if (joinProj.count(att.toString())==0) {
                joinProj.insert(att.toString());
                auto type = allTables[att.getTable()]->getTable()->getSchema()->getType(att.toString());
                mySchemaOut->appendAtt (make_pair (att.getTable() + "_" + att.getAtt(), type));
                proj.push_back(att.toString());
            }
        }
        
        //        for (auto joinPred: joinPreds) {
        //            if (joinPred.first == tblPair) {
        //                continue;
        //            }
        //            for (auto pred:joinPred.second) {
        //                for (auto att : pred->getAtomicIdentifiers()) {
        //                    if (att.getTable() == tblPair.first) {
        //                        auto type = leftTbl->getTable()->getSchema()->getType(att.toString());
        //                        mySchemaOut->appendAtt (make_pair (att.getTable() + "_" + att.getAtt(), type));
        //                    } else if (att.getTable() == tblPair.second) {
        //                        auto type = rightTbl->getTable()->getSchema()->getType(att.toString());
        //                        mySchemaOut->appendAtt (make_pair (att.getTable() + "_" + att.getAtt(), type));
        //                    }
        //                }
        //            }
        //        }
        
        //        cout <<"start making equalityChecks!" <<endl;
        
        vector <pair <string, string>> equalityChecks;
        string finalPred = getPredicateString(tblAtt->second);
        for (auto pred:tblAtt->second) {
            if (pred->toString().find("==") == 0) {
                auto v = pred->getAtomicIdentifiers();
                auto att1 = *(v.begin());
                auto att2 = *(next(v.begin(), 1));
                if (att1.getTable()==tblPair.first) {
                    equalityChecks.push_back(make_pair(att1.toString(), att2.toString()));
                } else {
                    equalityChecks.push_back(make_pair(att2.toString(), att1.toString()));
                }
            }
        }
        
        cout << "final predicate: "<<finalPred <<endl;
        
        for (auto eq:equalityChecks) {
            cout << "left att: " << eq.first << " equal to right att: " << eq.second << endl;
        }
        
        
        auto myTableOutRW = getTempTableRW(mySchemaOut);
        auto tempName = myTableOutRW->getTable()->getName();
        
        //        cout << "start join! "<< endl;
        cout << "left schema: " << leftTbl->getTable()->getSchema()<< endl;
        cout << "right schema: " << rightTbl->getTable()->getSchema()<< endl;
        cout <<"output schema: "<< mySchemaOut <<endl;
        
        //
        //        for(auto a: proj) {
        //            cout << "proj has: "<< a <<endl;
        //        }
        if (chooseJoin(leftTbl, rightTbl, equalityChecks)   ) {
            cout << "\nusing scan join! "<< endl;
            equalityChecks.push_back(make_pair("bool[true]", "bool[true]"));
            auto myOp = ScanJoin(leftTbl, rightTbl, myTableOutRW, finalPred, proj, equalityChecks, leftPred, rightPred);
            myOp.run();
        } else {
            cout << "\nusing sort-merge join! "<< endl;
            auto myOp = SortMergeJoin(leftTbl, rightTbl, myTableOutRW, finalPred, proj, equalityChecks[0], leftPred, rightPred);
            myOp.run();
        }
        
        //        printResult(myTableOutRW);
        joinTableMapping[joinTableMapping[leftTbl->getTable()->getName()]] = tempName;
        joinTableMapping[joinTableMapping[rightTbl->getTable()->getName()]] = tempName;
        joinTableMapping[leftTbl->getTable()->getName()] = tempName;
        joinTableMapping[rightTbl->getTable()->getName()] = tempName;
        joinTableMapping[tempName] = tempName;
        tblAtt = joinPreds.erase(tblAtt);
    }
}

void RelAlgExpr :: finalSelAgg() {
    cout << "preparing selection.\n";
    // set up input table
    MyDB_TableReaderWriterPtr input = tempTables.size() > 0 ? tempTables[tempTables.size()-1] : allTables[inputQuery.tablesToProcess[0].second];
    //    cout << "input table is: "<< input->getTable()->getName() << endl;
    cout << "input table schema is: " << input->getTable()->getSchema()<< endl;
    
    // set up groupings
    vector<string> groupings;
    MyDB_SchemaPtr groupingSchema = make_shared <MyDB_Schema> ();
    initGrouping(input, groupings, groupingSchema);
    for (auto group: groupings) {
        //        cout << "grouping is: "<< group << endl;
    }
    
    // set up selection
    vector<string> projections;
    vector<pair<MyDB_AggType, string>> aggsToCompute;
    MyDB_SchemaPtr mySchemaOut = make_shared <MyDB_Schema> ();
    initSelection(input, mySchemaOut, groupingSchema, projections, aggsToCompute);
    
    // set up output table
    MyDB_TableReaderWriterPtr output = getTempTableRW(mySchemaOut);
    //    cout << "output table is: "<< output->getTable()->getName() << endl;
    cout << "output schema is: "<< mySchemaOut<< endl;
    
    // set up predicate
    vector<ExprTreePtr> finalPreds;
    for (auto preds: tableSelectionMap) {
        finalPreds.insert(finalPreds.end(), preds.second.begin(), preds.second.end());
    }
    
    string selectionPredicate = getPredicateString(finalPreds);
    cout << "predicate is: "<< selectionPredicate << endl;
    
    // run SQL!
    //    cout << "preparing final selection.\n";
    if (groupings.size() > 0) {
        MyDB_TablePtr temp = make_shared <MyDB_Table> ("temp_groupings", "temp_groupings.bin", groupingSchema);
        MyDB_TableReaderWriterPtr groupingTable = make_shared <MyDB_TableReaderWriter> (temp, myMgr);
        
        cout << "running aggregation.\n";
        
        Aggregate myOp1(input, groupingTable, aggsToCompute, groupings, selectionPredicate);
        myOp1.run();
        
        cout << "running selection.\n";
        
        RegularSelection myOp2 (groupingTable, output, "bool[true]", projections);
        myOp2.run ();
        
    } else if (aggsToCompute.size() > 0) {
        cout << "running aggregation.\n";
        Aggregate myOp(input, output, aggsToCompute, groupings, selectionPredicate);
        myOp.run();
        
    } else {
        cout << "running selection.\n";
        
        RegularSelection myOp (input, output, selectionPredicate, projections);
        myOp.run ();
    }
    
    // print out the first 30 query results
    printResult(output);
}

string RelAlgExpr :: getPredicateString(vector <ExprTreePtr> disjunction) {
    if (disjunction.size() == 0) {
        return "bool[true]";
    }
    string ans = "";
    for (ExprTreePtr a : disjunction) {
        if (ans == "") {
            ans = a->toString();
        } else {
            ans = "&& ("+ans+", "+ a->toString()+")";
        }
    }
    return ans;
}

void RelAlgExpr :: initGrouping(MyDB_TableReaderWriterPtr input,
                                vector<string> &groupings,
                                MyDB_SchemaPtr &groupingSchema) {
    for (ExprTreePtr a : inputQuery.groupingClauses) {
        string projectionName = a->toString();
        MyDB_SchemaPtr schema = input->getTable()->getSchema();
        MyDB_AttTypePtr attType = schema->getType(projectionName);
        groupings.push_back(projectionName);
        string attName = projectionName.substr(1, projectionName.length()-2);
        groupingSchema->appendAtt(make_pair(attName, attType));
    }
}

void RelAlgExpr :: initSelection(MyDB_TableReaderWriterPtr input,
                                 MyDB_SchemaPtr &mySchemaOut,
                                 MyDB_SchemaPtr &groupingSchema,
                                 vector<string> &projections,
                                 vector<pair<MyDB_AggType, string>> &aggsToCompute){
    int i = 0;
    int j = 0;
    for (ExprTreePtr a : inputQuery.valuesToSelect) {
        // set up output schema
        string projectionName = a->toString();
        MyDB_AttTypePtr attType = input->getTable()->getSchema()->getType(projectionName);
        mySchemaOut->appendAtt(make_pair(projectionName, attType));
        
        // set up aggregation and projections
        if (a->getType() == SUMOP) {
            projectionName = projectionName.substr(4, projectionName.length()-5);
            aggsToCompute.push_back(make_pair(MyDB_AggType::Sum, projectionName));
            
            string attName = "sum" + to_string(i++);
            groupingSchema->appendAtt(make_pair(attName, attType));
            projections.push_back("["+attName+"]");
        } else if (a->getType() == AVGOP) {
            projectionName = projectionName.substr(4, projectionName.length()-5);
            aggsToCompute.push_back(make_pair(MyDB_AggType::Avg, projectionName));
            
            string attName = "avg" + to_string(j++);
            groupingSchema->appendAtt(make_pair(attName, attType));
            projections.push_back("["+attName+"]");
        } else {
            projections.push_back(projectionName);
        }
    }
}

void RelAlgExpr :: printResult(MyDB_TableReaderWriterPtr output) {
    cout << "\n";
    MyDB_RecordPtr temp = output->getEmptyRecord();
    auto myiter = output->getIteratorAlt();
    int counter = 0;
    while (myiter->advance()) {
        myiter->getCurrent(temp);
        if (counter<outputLimit) {
            cout << temp << endl;
        }
        counter++;
    }
    cout << "total records: " << counter << endl;
}

MyDB_TableReaderWriterPtr RelAlgExpr:: renameTable(MyDB_TableReaderWriterPtr inTable, pair<string, string> alias) {
    MyDB_SchemaPtr newSchema = make_shared <MyDB_Schema> ();
    auto oldSchema = inTable->getTable()->getSchema();
    int attCount = 0;
    for (auto att : oldSchema->getAtts()) {
        newSchema->appendAtt(make_pair(alias.second + "_" + att.first, att.second));
        attCount++;
    }
    MyDB_TablePtr newTable = make_shared <MyDB_Table> (alias.first, inTable->getTable()->getStorageLoc(), newSchema, inTable->getTable()->getFileType(), inTable->getTable()->getSortAtt());
    newTable->setLastPage(inTable->getTable()->lastPage());
    newTable->setTupleCount(inTable->getTable()->getTupleCount());
    newTable->setRootLocation(inTable->getTable()->getRootLocation());
    int i;
    vector<size_t> newValueCount;
    for (i = 0; i<attCount; i++) {
        newValueCount.push_back(inTable->getTable()->getDistinctValues(i));
    }
    newTable->setDistinctValues(newValueCount);
    MyDB_TableReaderWriterPtr newTableRW = make_shared <MyDB_TableReaderWriter> (newTable, myMgr);
    return newTableRW;
}

MyDB_TableReaderWriterPtr RelAlgExpr:: getTempTableRW(MyDB_SchemaPtr schema) {
    auto tempName = "temp_"+to_string(tempTables.size());
    MyDB_TablePtr myTableOut = make_shared <MyDB_Table> (tempName, tempName+".bin", schema);
    MyDB_TableReaderWriterPtr myTableOutRW = make_shared <MyDB_TableReaderWriter> (myTableOut, myMgr);
    tempTables.push_back(myTableOutRW);
    allTables[tempName] = myTableOutRW;
    return myTableOutRW;
}

string RelAlgExpr :: findTableName(string input) {
    while (joinTableMapping[input] != input) {
        input = joinTableMapping[input];
    }
    return input;
}

vector<pair<pair<string,string>, vector<ExprTreePtr>>> RelAlgExpr:: sortJoinPred(map<pair<string,string>, vector<ExprTreePtr>> joinPreds) {
    vector<pair<long, pair<string,string>>> tableCount;
    
    for (auto tables: joinPreds) {
        long count = allTables[tables.first.first]->getTable()->getTupleCount() * allTables[tables.first.second]->getTable()->getTupleCount();
        tableCount.push_back(make_pair(count, tables.first));
    }
    
    sort(tableCount.begin(), tableCount.end());
    
    vector<pair<pair<string,string>, vector<ExprTreePtr>>> res;
    for (auto tables = tableCount.rbegin(); tables != tableCount.rend(); tables++) {
        res.push_back(make_pair(tables->second, joinPreds[tables->second]));
    }
    return res;
}

bool RelAlgExpr :: chooseJoin(MyDB_TableReaderWriterPtr left, MyDB_TableReaderWriterPtr right, vector <pair <string, string>> equalityChecks) {
    MyDB_TableReaderWriterPtr small, large;
    if (left->getNumPages() > right->getNumPages()) {
        small = right;
        large = left;
    } else {
        small = left;
        large = right;
    }
    if (equalityChecks.size() == 0) {
        return true;
    }
    if (small->getNumPages() > small->getBufferMgr()->numPages) {
        return false;
    }
    if (large->getNumPages() > 2 * small->getNumPages()) {
        return true;
    } else {
        return false;
    }
    
}

