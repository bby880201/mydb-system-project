//
//  RelAlgExpr.h
//  A7
//
//  Created by Boyang Bai on 4/27/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#ifndef RelAlgExpr_h
#define RelAlgExpr_h

#include <stdio.h>
#include "MyDB_AttType.h"
#include "MyDB_Catalog.h"
#include "MyDB_Schema.h"
#include "MyDB_Table.h"
#include "MyDB_TableReaderWriter.h"
#include "MyDB_BPlusTreeReaderWriter.h"
#include "ParserTypes.h"
#include "RegularSelection.h"
#include "Aggregate.h"
#include <string>

// structure that sores an entire relational algebra expression

class RelAlgExpr {
    
public:
    
    RelAlgExpr (map <string, MyDB_TableReaderWriterPtr> loadedTables, SFWQuery query, MyDB_BufferManagerPtr bufMgr, MyDB_CatalogPtr cat);
    
    ~RelAlgExpr ();
    void run ();
    
private:
    SFWQuery inputQuery;
    MyDB_CatalogPtr myCatFile;
    map <string, MyDB_TableReaderWriterPtr> allTables;
    MyDB_BufferManagerPtr myMgr;
    vector <MyDB_TableReaderWriterPtr> tempTables;
    int outputLimit = 30;
    map <string, string> joinTableMapping;
    map<string, vector<Identifier>> tableAttsMap;
    vector<pair<pair<string,string>, vector<ExprTreePtr>>> joinPreds;
    map<string, vector<ExprTreePtr>> tableSelectionMap;
    map<string, ExprTreePtr> multiTblProjec;
    
    
    void initGrouping(MyDB_TableReaderWriterPtr input,
                      vector<string> &groupings,
                      MyDB_SchemaPtr &groupingSchema);
    void initSelection(MyDB_TableReaderWriterPtr input,
                       MyDB_SchemaPtr &mySchemaOut,
                       MyDB_SchemaPtr &groupingSchema,
                       vector<string> &projections,
                       vector<pair<MyDB_AggType, string>> &aggsToCompute);
    void init(map <string, MyDB_TableReaderWriterPtr> loadedTables);
    void checkJoin();
    void finalSelAgg();
    void printResult(MyDB_TableReaderWriterPtr output);
    MyDB_TableReaderWriterPtr renameTable(MyDB_TableReaderWriterPtr inTable, pair<string, string> alias);
    MyDB_TableReaderWriterPtr getTempTableRW(MyDB_SchemaPtr schema);
    string findTableName(string input);
    string getPredicateString(vector <ExprTreePtr> disjunctions);
    vector<pair<pair<string,string>, vector<ExprTreePtr>>> sortJoinPred(map<pair<string,string>, vector<ExprTreePtr>> joinPreds);
    bool chooseJoin(MyDB_TableReaderWriterPtr left, MyDB_TableReaderWriterPtr right, vector <pair <string, string>> equalityChecks);
};




#endif /* RelAlgExpr_h */
