
#ifndef SQL_EXPRESSIONS
#define SQL_EXPRESSIONS

#include "MyDB_AttType.h"
#include <set>
#include <string>
#include <vector>

// create a smart pointer for database tables
using namespace std;
class ExprTree;
class Identifier;
typedef shared_ptr <ExprTree> ExprTreePtr;
//typedef shared_ptr <Identifier> Identifier;


enum ExprTreeType {
    BOOLLITERAL,
    DOUBLELITERAL,
    INTLITERAL,
    STRINGLITERAL,
    ID,
    MINUSOP,
    PLUSOP,
    TIMESOP,
    DIVIDEOP,
    GTOP,
    LTOP,
    NEQOP,
    OROP,
    EQOP,
    NOTOP,
    SUMOP,
    AVGOP
};


// this class encapsules a parsed SQL expression (such as "this.that > 34.5 AND 4 = 5")

// class ExprTree is a pure virtual class... the various classes that implement it are below
class ExprTree {

public:
	virtual string toString () = 0;
    virtual vector<Identifier> getAtomicIdentifiers() = 0;
    virtual set<string> tableToJoin() = 0;
    virtual ExprTreeType getType() = 0;
    virtual string toStringWithoutTableName () = 0;
	virtual ~ExprTree () {}
};

class BoolLiteral : public ExprTree {

private:
	bool myVal;
public:
	
	BoolLiteral (bool fromMe) {
		myVal = fromMe;
	}

	string toString () {
		if (myVal) {
			return "bool[true]";
		} else {
			return "bool[false]";
		}
	}
    
    vector<Identifier> getAtomicIdentifiers() {
        return vector<Identifier>();
    }
    
    set<string> tableToJoin() {
        return set<string>();
    }
    
    string toStringWithoutTableName() {
        return toString();
    }
    ExprTreeType getType() {
        return BOOLLITERAL;
    }
};

class DoubleLiteral : public ExprTree {

private:
	double myVal;
public:

	DoubleLiteral (double fromMe) {
		myVal = fromMe;
	}

	string toString () {
		return "double[" + to_string (myVal) + "]";
	}
    
    vector<Identifier> getAtomicIdentifiers() {
        return vector<Identifier>();
    }
    
    set<string> tableToJoin() {
        return set<string>();
    }
    
    string toStringWithoutTableName() {
        return toString();
    }

    ExprTreeType getType() {
        return DOUBLELITERAL;
    }
    
	~DoubleLiteral () {}
};

// this implement class ExprTree
class IntLiteral : public ExprTree {

private:
	int myVal;
public:

	IntLiteral (int fromMe) {
		myVal = fromMe;
	}

	string toString () {
		return "int[" + to_string (myVal) + "]";
	}
    
    vector<Identifier> getAtomicIdentifiers() {
        return vector<Identifier>();
    }
    
    set<string> tableToJoin() {
        return set<string>();
    }
    
    string toStringWithoutTableName() {
        return toString();
    }

    ExprTreeType getType() {
        return INTLITERAL;
    }
    
	~IntLiteral () {}
};

class StringLiteral : public ExprTree {

private:
	string myVal;
public:

	StringLiteral (char *fromMe) {
		fromMe[strlen (fromMe) - 1] = 0;
		myVal = string (fromMe + 1);
	}

	string toString () {
		return "string[" + myVal + "]";
	}
    
    vector<Identifier> getAtomicIdentifiers() {
        return vector<Identifier>();
    }
    
    set<string> tableToJoin() {
        return set<string>();
    }

    string toStringWithoutTableName() {
        return toString();
    }
    
    ExprTreeType getType() {
        return STRINGLITERAL;
    }
    
	~StringLiteral () {}
};

class Identifier : public ExprTree {

private:
	string tableName;
	string attName;
public:

	Identifier (char *tableNameIn, char *attNameIn) {
		tableName = string (tableNameIn);
		attName = string (attNameIn);
	}
    
    string getTable() {
        return tableName;
    }
    
    string getAtt() {
        return attName;
    }

    string toString () {
        return "[" + tableName + "_" + attName + "]";
    }
    
    string toStringWithoutTableName() {
        return "[" + attName + "]";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        vector<Identifier> v1;
        v1.push_back(*this);
        return v1;
    }
    
    set<string> tableToJoin() {
        set<string> v1;
        v1.insert(tableName);
        return v1;
    }
    
    ExprTreeType getType() {
        return ID;
    }
    
	~Identifier () {}
};

class MinusOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	MinusOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "- (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }
    
    string toStringWithoutTableName() {
        return  "- (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    ExprTreeType getType() {
        return MINUSOP;
    }
    
	~MinusOp () {}
};

class PlusOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	PlusOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "+ (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "+ (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }

    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return PLUSOP;
    }
    
	~PlusOp () {}
};

class TimesOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	TimesOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "* (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    string toStringWithoutTableName() {
        return "* (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }

    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }

    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }
    
    ExprTreeType getType() {
        return TIMESOP;
    }
    
	~TimesOp () {}
};

class DivideOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	DivideOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "/ (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "/ (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }

    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }
    
    ExprTreeType getType() {
        return DIVIDEOP;
    }
    
	~DivideOp () {}
};

class GtOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	GtOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "> (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "> (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return GTOP;
    }
    
	~GtOp () {}
};

class LtOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	LtOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "< (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "< (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return LTOP;
    }
    
	~LtOp () {}
};

class NeqOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	NeqOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "!= (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "!= (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return NEQOP;
    }
    
	~NeqOp () {}
};

class OrOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	OrOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "|| (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "|| (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return OROP;
    }
    
	~OrOp () {}
};

class EqOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	EqOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
	}

	string toString () {
		return "== (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "== (" + lhs->toStringWithoutTableName () + ", " + rhs->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        auto v1 = lhs->getAtomicIdentifiers();
        auto v2 = rhs->getAtomicIdentifiers();
        v1.insert( v1.end(), v2.begin(), v2.end() );
        return v1;
    }
    
    set<string> tableToJoin() {
        auto v1 = lhs->tableToJoin();
        auto v2 = rhs->tableToJoin();
        v1.insert(v2.begin(), v2.end());
        return v1;
    }

    ExprTreeType getType() {
        return EQOP;
    }
    
	~EqOp () {}
};

class NotOp : public ExprTree {

private:

	ExprTreePtr child;
	
public:

	NotOp (ExprTreePtr childIn) {
		child = childIn;
	}

	string toString () {
		return "!(" + child->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "!(" + child->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        return child->getAtomicIdentifiers();
    }
    
    set<string> tableToJoin() {
        return child->tableToJoin();
    }

    ExprTreeType getType() {
        return NOTOP;
    }
    
	~NotOp () {}
};

class SumOp : public ExprTree {

private:

	ExprTreePtr child;
	
public:

	SumOp (ExprTreePtr childIn) {
		child = childIn;
	}

	string toString () {
		return "sum(" + child->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "sum(" + child->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        return child->getAtomicIdentifiers();
    }
    
    set<string> tableToJoin() {
        return child->tableToJoin();
    }

    ExprTreeType getType() {
        return SUMOP;
    }
    
	~SumOp () {}
};

class AvgOp : public ExprTree {

private:

	ExprTreePtr child;
	
public:

	AvgOp (ExprTreePtr childIn) {
		child = childIn;
	}

	string toString () {
		return "avg(" + child->toString () + ")";
	}
    
    string toStringWithoutTableName() {
        return "avg(" + child->toStringWithoutTableName () + ")";
    }
    
    vector<Identifier> getAtomicIdentifiers() {
        return child->getAtomicIdentifiers();
    }
    
    set<string> tableToJoin() {
        return child->tableToJoin();
    }

    ExprTreeType getType() {
        return AVGOP;
    }
    
	~AvgOp () {}
};

#endif
