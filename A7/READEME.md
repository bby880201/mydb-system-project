Team: 

 * Boyang Bai (bb26)
 * Kunfeng Qiu (kq2)
 
 
Please select "5" (SQL Parser) when using scons to build our program to run the DBMS.

For extra credit, we implemented the join methods choosing feature. When join two table, if both table can fit into memory and one is twice lager than the other one, we will use ScanJoin, otherwise, we will use SortMergeJoin.