
#ifndef SQL_EXPRESSIONS
#define SQL_EXPRESSIONS

#include "MyDB_AttType.h"
#include "MyDB_Catalog.h"
#include <string>
#include <vector>

// create a smart pointer for database tables
using namespace std;
class ExprTree;
typedef shared_ptr <ExprTree> ExprTreePtr;

// this class encapsules a parsed SQL expression (such as "this.that > 34.5 AND 4 = 5")

// class ExprTree is a pure virtual class... the various classes that implement it are below
class ExprTree {
    
public:
    string exprType;
	virtual string toString () = 0;
    virtual bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) = 0;
    virtual bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) = 0;
    virtual MyDB_AttTypePtr getAttType(string &error) = 0;
	virtual ~ExprTree () {}
};


class BoolLiteral : public ExprTree {

private:
    
	bool myVal;
    
public:
	
	BoolLiteral (bool fromMe) {
		myVal = fromMe;
        exprType = "Bool";
	}

	string toString () {
		if (myVal) {
			return "bool[true]";
		} else {
			return "bool[false]";
		}
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return true;
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        return MyDB_AttTypePtr(new MyDB_BoolAttType());
    }
    
};


class DoubleLiteral : public ExprTree {

private:
    
	double myVal;
    
public:

	DoubleLiteral (double fromMe) {
		myVal = fromMe;
        exprType = "Double";
	}

	string toString () {
		return "double[" + to_string (myVal) + "]";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return true;
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        return MyDB_AttTypePtr(new MyDB_DoubleAttType());
    }
    
	~DoubleLiteral () {}
};


class IntLiteral : public ExprTree {

private:

	int myVal;
    
public:

	IntLiteral (int fromMe) {
		myVal = fromMe;
        exprType = "Int";

	}

	string toString () {
		return "int[" + to_string (myVal) + "]";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return true;
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        return MyDB_AttTypePtr(new MyDB_IntAttType());
    }
    
	~IntLiteral () {}
};


class StringLiteral : public ExprTree {

private:
    
	string myVal;
    
public:

	StringLiteral (char *fromMe) {
		fromMe[strlen (fromMe) - 1] = 0;
		myVal = string (fromMe + 1);
        exprType = "String";
	}

	string toString () {
		return "string[" + myVal + "]";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return true;
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        return MyDB_AttTypePtr(new MyDB_StringAttType());
    }

	~StringLiteral () {}
};


class Identifier : public ExprTree {

private:
    
	string tableName;
	string attName;
    MyDB_AttTypePtr attType;
    
public:

	Identifier (char *tableNameIn, char *attNameIn) {
		tableName = string (tableNameIn);
		attName = string (attNameIn);
        exprType = "Attribute";
        attType = nullptr;
	}

	string toString () {
		return "[" + tableName + "_" + attName + "]";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        for (auto tbl : tables) {
            if (tbl.first == tableName || tbl.second == tableName) {
                vector<string> attList;
                catFile->getStringList(tbl.first+".attList", attList);
                for (auto att : attList) {
                    if (att == attName) {
                        string type;
                        catFile->getString(tbl.first+"."+att+".type",type);
                        if (type == "string") {
                            attType = MyDB_AttTypePtr(new MyDB_StringAttType());
                        } else if (type == "int") {
                            attType = MyDB_AttTypePtr(new MyDB_IntAttType());
                        } else if (type == "double") {
                            attType = MyDB_AttTypePtr(new MyDB_DoubleAttType());
                        } else if (type == "bool") {
                            attType = MyDB_AttTypePtr(new MyDB_BoolAttType());
                        } else {
                            error = "attribute '" + attName + "' has undefined type '" + type + "'";
                            return false;
                        }
                        return true;
                    }
                }
                error = "cannot find attribute '" + attName + "' from table '" + tableName + "'";
                return false;
            }
        }
        error = "cannot find table '" + tableName + "'";
        return false;
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        for (auto att: groupbyAtts){
            if (att->toString() == toString()) {
                return true;
            }
        }
        error = "selected attribute '" + toString() + "' is not a functions of the grouping attributes";
        return false;
    }

    MyDB_AttTypePtr getAttType(string &error) {
        return attType;
    }

	~Identifier () {}
};


class MinusOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	MinusOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Minus";
	}

	string toString () {
		return "- (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }

    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if (lhsType->promotableToDouble() && rhsType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else {
            error = "'-' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }
    
	~MinusOp () {}
};


class PlusOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	PlusOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Plus";
	}

	string toString () {
		return "+ (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if (lhsType->promotableToDouble() && rhsType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else if (lhsType->toString() == "string" && rhsType->toString() == "string") {
            return MyDB_AttTypePtr(new MyDB_StringAttType());
        } else {
            error = "'+' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

 	~PlusOp () {}
};


class TimesOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	TimesOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Times";
	}

	string toString () {
		return "* (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if (lhsType->promotableToDouble() && rhsType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else {
            error = "'*' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

 
    ~TimesOp () {}
};


class DivideOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	DivideOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Divide";
	}

	string toString () {
		return "/ (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if (lhsType->promotableToDouble() && rhsType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else {
            error = "'/' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }
    
	~DivideOp () {}
};


class GtOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	GtOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Gt";
	}

	string toString () {
		return "> (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if ((lhsType->promotableToDouble() && rhsType->promotableToDouble()) ||
            (lhsType->toString() == "string" && rhsType->toString() == "string")){
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'>' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

	~GtOp () {}
};


class LtOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	LtOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Lt";
	}

	string toString () {
		return "< (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if ((lhsType->promotableToDouble() && rhsType->promotableToDouble()) ||
            (lhsType->toString() == "string" && rhsType->toString() == "string")){
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'<' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

	~LtOp () {}
};

class NeqOp : public ExprTree {
    
private:
    
    ExprTreePtr lhs;
    ExprTreePtr rhs;
    
public:
    
    NeqOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
        lhs = lhsIn;
        rhs = rhsIn;
        exprType = "Neq";
    }
    
    string toString () {
        return "!= (" + lhs->toString () + ", " + rhs->toString () + ")";
    }
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }

    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if ((lhsType->promotableToDouble() && rhsType->promotableToDouble()) ||
            (lhsType->toString() == "string" && rhsType->toString() == "string")||
            (lhsType->isBool() && rhsType->isBool())){
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'!=' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

    ~NeqOp () {}
};


class OrOp : public ExprTree {

private:
    
	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	OrOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Or";
	}

	string toString () {
		return "|| (" + lhs->toString () + ", " + rhs->toString () + ")";
	}	

    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if (lhsType->isBool() && rhsType->isBool()){
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'||' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

	~OrOp () {}
};


class EqOp : public ExprTree {

private:

	ExprTreePtr lhs;
	ExprTreePtr rhs;
	
public:

	EqOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
		lhs = lhsIn;
		rhs = rhsIn;
        exprType = "Eq";
	}

	string toString () {
		return "== (" + lhs->toString () + ", " + rhs->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return lhs->groupingCheck(error, groupbyAtts) && rhs->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr lhsType = lhs->getAttType(error);
        MyDB_AttTypePtr rhsType = rhs->getAttType(error);
        
        if (lhsType == nullptr || rhsType == nullptr) {
            return nullptr;
        }
        
        if ((lhsType->promotableToDouble() && rhsType->promotableToDouble()) ||
            (lhsType->toString() == "string" && rhsType->toString() == "string")||
            (lhsType->isBool() && rhsType->isBool())){
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'==' operation cannot be applied on '" + lhs->toString() + "' and '" + rhs->toString() + "'";
            return nullptr;
        }
    }

	~EqOp () {}
};

class NotOp : public ExprTree {

private:
    
	ExprTreePtr child;
	
public:

	NotOp (ExprTreePtr childIn) {
		child = childIn;
        exprType = "Not";
	}

	string toString () {
		return "!(" + child->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return child->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return child->groupingCheck(error, groupbyAtts);
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr childType = child->getAttType(error);
        
        if (childType == nullptr) {
            return nullptr;
        }
        
        if (childType->isBool()) {
            return MyDB_AttTypePtr(new MyDB_BoolAttType());
        } else {
            error = "'!' operation cannot be applied on '" + child->toString() + "'";
            return nullptr;
        }
    }

	~NotOp () {}
};


class SumOp : public ExprTree {

private:
    
	ExprTreePtr child;
	
public:
    
	SumOp (ExprTreePtr childIn) {
		child = childIn;
        exprType = "Sum";
	}

	string toString () {
		return "sum(" + child->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return child->selfCheck(error, catFile, tables);
    }
    
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr childType = child->getAttType(error);
        
        if (childType == nullptr) {
            return nullptr;
        }
        
        if (childType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else {
            error = "'SUM' operation cannot be applied on '" + child->toString() + "'";
            return nullptr;
        }
    }


	~SumOp () {}
};


class AvgOp : public ExprTree {

private:
    
	ExprTreePtr child;
	
public:
    
	AvgOp (ExprTreePtr childIn) {
		child = childIn;
        exprType = "Avg";
	}

	string toString () {
		return "avg(" + child->toString () + ")";
	}
    
    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
        return child->selfCheck(error, catFile, tables);
    }
    
    bool groupingCheck(string &error, vector<ExprTreePtr> groupbyAtts) {
        return true;
    }
    
    MyDB_AttTypePtr getAttType(string &error) {
        MyDB_AttTypePtr childType = child->getAttType(error);
        
        if (childType == nullptr) {
            return nullptr;
        }
        
        if (childType->promotableToDouble()) {
            return MyDB_AttTypePtr(new MyDB_DoubleAttType());
        } else {
            error = "'AVG' operation cannot be applied on '" + child->toString() + "'";
            return nullptr;
        }
    }

	~AvgOp () {}
};

//class AndOp : public ExprTree {
//    
//private:
//
//    ExprTreePtr lhs;
//    ExprTreePtr rhs;
//    
//public:
//    
//    AndOp (ExprTreePtr lhsIn, ExprTreePtr rhsIn) {
//        lhs = lhsIn;
//        rhs = rhsIn;
//        isAgg = true;
//    }
//    
//    string toString () {
//        return "&& (" + lhs->toString () + ", " + rhs->toString () + ")";
//    }
//    
//    bool selfCheck(string &error, MyDB_CatalogPtr catFile, vector <pair <string, string>> tables) {
//        return lhs->selfCheck(error, catFile, tables) && rhs->selfCheck(error, catFile, tables);
//    }
//    
//    ~AndOp () {}
//};


#endif
