
#ifndef SORT_C
#define SORT_C

#include <queue>
#include "MyDB_PageListIteratorAlt.h"
#include "MyDB_PageReaderWriter.h"
#include "MyDB_TableRecIterator.h"
#include "MyDB_TableRecIteratorAlt.h"
#include "MyDB_TableReaderWriter.h"
#include "IteratorComparator.h"
#include "Sorting.h"

using namespace std;

void mergeIntoFile (MyDB_TableReaderWriter &sortIntoMe, vector <MyDB_RecordIteratorAltPtr> &mergeUs, 
	function <bool ()> comparator, MyDB_RecordPtr lhs, MyDB_RecordPtr rhs) {

	// create the comparator and the priority queue
	IteratorComparator temp (comparator, lhs, rhs);
	priority_queue <MyDB_RecordIteratorAltPtr, vector <MyDB_RecordIteratorAltPtr>, IteratorComparator> pq (temp);

	// load up the set
	for (MyDB_RecordIteratorAltPtr m : mergeUs) {
		if (m->advance ()) {
			pq.push (m);
		}
	}

	// and write everyone out
	int counter = 0;
	while (pq.size () != 0) {

		// write the dude to the output
		auto myIter = pq.top ();
		myIter->getCurrent (lhs);
		sortIntoMe.append (lhs);
		counter++;

		// remove from the q
		pq.pop ();

		// re-insert
		if (myIter->advance ()) {
			pq.push (myIter);
		}
	}
}

void appendRecord (MyDB_PageReaderWriter &curPage, vector <MyDB_PageReaderWriter> &returnVal, 
	MyDB_RecordPtr appendMe, MyDB_BufferManagerPtr parent) {

	// try to append to the current page
	if (!curPage.append (appendMe)) {

		// if we cannot, then add a new one to the output vector
		returnVal.push_back (curPage);
		MyDB_PageReaderWriter temp (*parent);
		temp.append (appendMe);
		curPage = temp;
	}
}

vector <MyDB_PageReaderWriter> mergeIntoList (MyDB_BufferManagerPtr parent, MyDB_RecordIteratorAltPtr leftIter, 
	MyDB_RecordIteratorAltPtr rightIter, function <bool ()> comparator, MyDB_RecordPtr lhs, MyDB_RecordPtr rhs) {
	
	vector <MyDB_PageReaderWriter> returnVal;
	MyDB_PageReaderWriter curPage (*parent);
	bool lhsLoaded = false, rhsLoaded = false;

	// if one of the runs is empty, get outta here
	if (!leftIter->advance ()) {
		while (rightIter->advance ()) {
			rightIter->getCurrent (rhs);
			appendRecord (curPage, returnVal, rhs, parent);
		}
	} else if (!rightIter->advance ()) {
		while (leftIter->advance ()) {
			leftIter->getCurrent (lhs);
			appendRecord (curPage, returnVal, lhs, parent);
		}
	} else {
		while (true) {
	
			// get the two records

			// here's a bit of an optimization... if one of the records is loaded, don't re-load
			if (!lhsLoaded) {
				leftIter->getCurrent (lhs);
				lhsLoaded = true;
			}

			if (!rhsLoaded) {
				rightIter->getCurrent (rhs);		
				rhsLoaded = true;
			}
	
			// see if the lhs is less
			if (comparator ()) {
				appendRecord (curPage, returnVal, lhs, parent);
				lhsLoaded = false;

				// deal with the case where we have to append all of the right records to the output
				if (!leftIter->advance ()) {
					appendRecord (curPage, returnVal, rhs, parent);
					while (rightIter->advance ()) {
						rightIter->getCurrent (rhs);
						appendRecord (curPage, returnVal, rhs, parent);
					}
					break;
				}
			} else {
				appendRecord (curPage, returnVal, rhs, parent);
				rhsLoaded = false;

				// deal with the ase where we have to append all of the right records to the output
				if (!rightIter->advance ()) {
					appendRecord (curPage, returnVal, lhs, parent);
					while (leftIter->advance ()) {
						leftIter->getCurrent (lhs);
						appendRecord (curPage, returnVal, lhs, parent);
					}
					break;
				}
			}
		}
	}
	
	// remember the current page
	returnVal.push_back (curPage);
	
	// outta here!
	return returnVal;
}


vector<MyDB_RecordIteratorAltPtr> merge(vector<MyDB_RecordIteratorAltPtr> pgIterList, MyDB_BufferManagerPtr parent, function <bool ()> comparator, MyDB_RecordPtr lhs, MyDB_RecordPtr rhs) {
    
    // base case
    if (pgIterList.size() == 1) {
        return pgIterList;
    }
    
    // recursion
    vector<MyDB_RecordIteratorAltPtr> mergedList;
    
    // while more than one left, merge pages two by two
    while (pgIterList.size() > 1) {
        MyDB_RecordIteratorAltPtr pg1 = pgIterList.back();
        pgIterList.pop_back();
        MyDB_RecordIteratorAltPtr pg2 = pgIterList.back();
        pgIterList.pop_back();
        
        vector <MyDB_PageReaderWriter> tempList = mergeIntoList(parent, pg1, pg2, comparator, lhs, rhs);
        MyDB_RecordIteratorAltPtr pgIterPtr (new MyDB_PageListIteratorAlt(tempList));
        mergedList.push_back(pgIterPtr);
    }
    
    // if only one left, append it to mergedList
    if (pgIterList.size() == 1) {
        mergedList.push_back(pgIterList[0]);
        pgIterList.pop_back();
    }
    
    // recursively merge this mergedList 
    return merge(mergedList, parent, comparator, lhs, rhs);
}

void sort (int runSize, MyDB_TableReaderWriter & sortMe, MyDB_TableReaderWriter & sortIntoMe,
	function <bool ()> comparator, MyDB_RecordPtr lhs, MyDB_RecordPtr rhs) {
    
    // your stuff here!!
    
    // 1. Sort all recs in sortMe into runs:
    //
    // while sortMe not exhausted:
    //     load runSize pages into RAM
    //     use built-in function in MyDB_PageReaderWriter to sort each page
    //     use mergeIntoList to merge all pages into a list of pages
    //     write out as a run with runSize pages
    
    int curPageNum = 0;
    int totalPageNum = sortMe.getNumPages() - 1;
    MyDB_BufferManagerPtr bf = sortMe.getBufferMgr();
    
    vector<MyDB_RecordIteratorAltPtr> pgList, run, runList;
    while (curPageNum <= totalPageNum) {
        MyDB_PageReaderWriter curPage = sortMe[curPageNum];
        MyDB_PageReaderWriter temp = *curPage.sort(comparator, lhs, rhs);
        
        vector<MyDB_PageReaderWriter> runPages;
        runPages.push_back(temp);
        MyDB_RecordIteratorAltPtr pgIterPtr (new MyDB_PageListIteratorAlt(runPages));
        pgList.push_back(pgIterPtr);
        
        // if reach the runSize limit or end of file, write out a run
        if (curPageNum % runSize == runSize-1 || curPageNum == totalPageNum) {
            run = merge(pgList, bf, comparator, lhs, rhs);
            if (run.size()==1) {
                runList.push_back(run[0]);
                run.clear();
            }else {
                cerr<<"Wront run size!"<<endl;
            }
            pgList.clear();
        }
        curPageNum++;
    }
    mergeIntoFile(sortIntoMe, runList, comparator, lhs, rhs);

//    auto iterator = sortMe.getIteratorAlt(0, runSize);
//    
//    while (curPageNum<=totalPageNum) {
//        vector<MyDB_RecordIteratorAlt> pgListIter;
//        for (int i = 0; i<runSize; i++) {
//            if (curPageNum<=totalPageNum) {
//                MyDB_PageReaderWriter pgRW = MyDB_PageReaderWriter(sortMe, curPageNum);
//                pgRW.sort(comparator, lhs, rhs);
//                vector<MyDB_PageReaderWriter> pgRWVec;
//                pgRWVec.push_back(pgRW);
//                
//                MyDB_PageListIteratorAlt pgList = MyDB_PageListIteratorAlt(pgRWVec);
//                pgListIter.push_back(pgList);
//                curPageNum ++;
//            }
//            else {
//                break;
//            }
//        }
    
//        vector<MyDB_PageListIteratorAlt> megeredList;
//        while (pgListIter.size() > 1) {
//            MyDB_RecordIteratorAlt pg1 = pgListIter.back();
//            pgListIter.pop_back();
//            MyDB_PageListIteratorAlt pg2 = pgListIter.back();
//            pgListIter.pop_back();
//            auto pgMerged = mergeIntoList(bf, (MyDB_RecordIteratorAlt)pg1,(MyDB_RecordIteratorAlt) pg2, comparator, lhs, rhs);
//            
//        }
//
//        
//    }
    
    // 2. Merge runs
    //
    // use mergeIntoFile to merge runs
    
    
    // below is already implemented in mergeIntoFile
    //
    // load the 1st page of each run into RAM
    // insert all recs into a priority_queue
    // count[i] is run_i's recs number in RAM
    // while priority_queue != empty:
    //     remove smallest rec (run_x)
    //     write it into output_buffer
    //     if output_buffer is full: flush
    //     if count[x] == 0:
    //         if priority_queue != empty:
    //             load next page in run_x into priority_queue
    //             count[x] = number of recs in new page
    //         else:
    //             append rest of run_x to sortIntoMe

}

#endif
