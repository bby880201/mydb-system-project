#ifndef REG_SELECTION_C
#define REG_SELECTION_C

#include "MyDB_Record.h"
#include "MyDB_PageReaderWriter.h"
#include "MyDB_TableReaderWriter.h"
#include "Aggregate.h"
#include <unordered_map>

using namespace std;

Aggregate::Aggregate(MyDB_TableReaderWriterPtr input,
                     MyDB_TableReaderWriterPtr output,
                     vector <pair <MyDB_AggType, string>> aggsToComputeIn,
                     vector <string> groupingsIn,
                     string selectionPredicateIn) {
    inputTable = input;
    outputTable = output;
    aggsToCompute = aggsToComputeIn;
    groupings = groupingsIn;
    selectionPredicate = selectionPredicateIn;
}

void Aggregate::run(){
    
    // create a schema that stores all of the aggregate and grouping attributes
    MyDB_SchemaPtr aggSchema = make_shared <MyDB_Schema>();
    int i = 0;
    for (auto attr : outputTable->getTable()->getSchema()->getAtts()) {
        auto attr_type = attr.second;
        if (i < groupings.size()){
            aggSchema->appendAtt(make_pair("group" + to_string(i), attr_type));
        } else {
            aggSchema->appendAtt(make_pair("agg" + to_string(i-groupings.size()), attr_type));
        }
        i++;
    }
    aggSchema->appendAtt(make_pair("count", make_shared <MyDB_IntAttType>()));
    
    // and get the schema that results from combining the input and agg records
    MyDB_SchemaPtr combinedSchema = make_shared <MyDB_Schema> ();
    for (auto p : inputTable->getTable()->getSchema()->getAtts())
        combinedSchema->appendAtt(p);
    for (auto p : aggSchema->getAtts())
        combinedSchema->appendAtt(p);
    
    // get the input, agg, and combined record
    MyDB_RecordPtr inputRec = inputTable->getEmptyRecord();
    MyDB_RecordPtr aggRec = make_shared <MyDB_Record>(aggSchema);
    MyDB_RecordPtr combinedRec = make_shared <MyDB_Record>(combinedSchema);
    combinedRec->buildFrom (inputRec, aggRec);
    
    
    
    
    // functions to group (number) input records
    vector <func> groupInputFns;
    for (string comp : groupings) {
        groupInputFns.push_back (inputRec->compileComputation(comp));
    }
    
    // function to group (combine) combined records.
    string comp = "bool[true]";
    for (int i=0; i<groupings.size(); i++) {
        auto eq = "== (" + groupings[i] + ", [group" + to_string(i) + "])";
        comp = "&& (" + comp + ", " + eq + ")";
    }
    func groupCombinedFn = combinedRec->compileComputation(comp);
    
    // get the initial and final aggregate functions
    vector <func> agg1Fns;
    vector <func> agg2Fns;
    comp = "+ (int[1], [count])";
    agg1Fns.push_back(combinedRec->compileComputation(comp));
    for (int i=0; i<aggsToCompute.size(); i++) {
        auto agg = aggsToCompute[i];
        
        // if aggregation is SUM or AVERAGE
        if (agg.first == MyDB_AggType::sum || agg.first == MyDB_AggType::avg) {
            comp = "+ (" + agg.second + ", [agg" + to_string (i) + "])";
            
            // if aggregation is COUNT
        } else if (agg.first == MyDB_AggType::cnt) {
            comp = "+ (int[1], [agg" + to_string (i) + "])";
        }
        agg1Fns.push_back(combinedRec->compileComputation(comp));
        
        // if aggreation is AVERAGE,
        if (agg.first == MyDB_AggType::avg) {
            comp = "/ ([agg" + to_string (i) + "], [count])";
        } else {
            comp = "[agg" + to_string (i) + "]";
        }
        agg2Fns.push_back(combinedRec->compileComputation(comp));
    }
    
    
    
    
    // get the predicate
    func predicate = inputRec->compileComputation(selectionPredicate);
    
    // this is the hash map we'll use to look up data... the key is the hashed value
    // of all of the records' join keys, and the value is a list of pointers where all
    // of the records with that hash value are located
    unordered_map <size_t, vector <void *>> myHash;
    
    // anonymous pinned pages that stores all aggregate records.
    vector <MyDB_PageReaderWriter> pages;
    
    // current page to write
    MyDB_PageReaderWriter page(true, *(inputTable->getBufferMgr()));
    pages.push_back (page);
    
    // iterate through input table.
    MyDB_RecordIteratorPtr myIter = inputTable->getIterator (inputRec);
    while (myIter->hasNext ()) {
        
        myIter->getNext();
        
        // see if it is accepted by input predicate
        if (!predicate()->toBool ()) {
            continue;
        }
        
        // hash the current record
        size_t hashVal = 0;
        for (auto f : groupInputFns) {
            hashVal ^= f()->hash();
        }
        
        // get the list of potential matches... first verify that there IS
        // a match in there
        if (myHash.count (hashVal) == 0) {
            continue;
        }
        
        // if there is a match, then get the list of matches
        vector <void *> &potentialMatches = myHash [hashVal];
        
        // and iterate though the potential matches, checking each of them
        void *location = nullptr;
        for (auto v : potentialMatches) {
            
            // build the aggregate record
            aggRec->fromBinary (v);
            
            // find the match
            if (groupCombinedFn()->toBool ()) {
                location = v;
                break;
            }
        }
        
        // if no match found
        if (location == nullptr) {
            
            for (int i=0; i<groupInputFns.size(); i++) {
                aggRec->getAtt(i)->set(groupInputFns[i]());
            }
            for (int i=0; i<agg1Fns.size(); i++) {
                aggRec->getAtt(i)->set(make_shared <MyDB_IntAttVal>());
            }
        }
        
        for (int i=0; i<agg1Fns.size(); i++) {
            aggRec->getAtt(groupings.size()+i)->set(agg1Fns[i]());
        }
        
        // no match found, write to page
        aggRec->recordContentHasChanged();
        if (location == nullptr) {
            location = page.appendAndReturnLocation(aggRec);
            
            // can't write, go to next page
            if (location == nullptr) {
                MyDB_PageReaderWriter nextPage(true, *(inputTable->getBufferMgr()));
                page = nextPage;
                pages.push_back(page);
                location = page.appendAndReturnLocation(aggRec);
            }
            
            aggRec->fromBinary(location);
            myHash[hashVal].push_back(location);
            
        } else {
            aggRec->toBinary(location);
        }
    }
    
    MyDB_RecordIteratorAltPtr myIterAgain = getIteratorAlt(pages);
    
    // now, iterate through all aggregate records
    MyDB_RecordPtr outputRec = outputTable->getEmptyRecord ();
    while (myIterAgain->advance()) {
        
        myIterAgain->getCurrent(aggRec);
        
        for (int i=0; i<groupings.size(); i++) {
            outputRec->getAtt(i)->set(aggRec->getAtt(i));
        }
        
        for (int i=0; i<agg2Fns.size(); i++) {
            outputRec->getAtt(i)->set(agg2Fns[i]());
        }
        
        // the record's content has changed because it
        // is now a composite of two records whose content
        // has changed via a read... we have to tell it this,
        // or else the record's internal buffer may cause it
        // to write old values
        outputRec->recordContentHasChanged();
        outputTable->append(outputRec);
    }
}

#endif
