//
//  BPlusSelection.cc
//  A6
//
//  Created by Boyang Bai on 4/16/16.
//  Copyright © 2016 bb26. All rights reserved.
//

#include "BPlusSelection.h"

using namespace std;

BPlusSelection :: BPlusSelection (MyDB_BPlusTreeReaderWriterPtr input,
                                  MyDB_TableReaderWriterPtr output,
                                  MyDB_AttValPtr low,
                                  MyDB_AttValPtr high,
                                  string selectionPredicate,
                                  vector <string> projections) {
    inTable = input;
    outTable = output;
    lVal = low;
    hVal = high;
    selectPred = selectionPredicate;
    projection = projections;
}

void BPlusSelection :: run () {
    
    // prepare predicate function for record
    MyDB_RecordPtr inRec = inTable->getEmptyRecord ();
    func recPred = inRec->compileComputation (selectPred);
    
    // prepare output record and get the final set of computatoins that will be used to build the output record
    MyDB_RecordPtr outputRec = outTable->getEmptyRecord ();
    
    vector <func> finalComputations;
    for (string s : projection) {
        finalComputations.push_back (inRec->compileComputation (s));
    }

    // iterate over input records within query range
    MyDB_RecordIteratorAltPtr recRange = inTable->getRangeIteratorAlt(lVal, hVal);
    while (recRange->advance()) {
        //read record
        recRange->getCurrent(inRec);
        
        if (!recPred()->toBool()) {
            continue;
        }
        // select expected attributes to output record
        int i = 0;
        for (auto f : finalComputations) {
            outputRec->getAtt(i++)->set(f());
        }
        
        cout << outputRec;
        outputRec->recordContentHasChanged ();
        outTable->append (outputRec);
        
    }
}
