#ifndef REG_SELECTION_C
#define REG_SELECTION_C

#include "MyDB_Record.h"
#include "MyDB_PageReaderWriter.h"
#include "MyDB_TableReaderWriter.h"
#include "RegularSelection.h"

using namespace std;

RegularSelection::RegularSelection(MyDB_TableReaderWriterPtr inputIn,
                                   MyDB_TableReaderWriterPtr outputIn,
                                   string selectionPredicateIn,
                                   vector <string> projectionsIn) {
    inputTable = inputIn;
    outputTable = outputIn;
    selectionPredicate = selectionPredicateIn;
    projections = projectionsIn;
}

void RegularSelection::run(){
    
    // get the input record
    MyDB_RecordPtr inputRec = inputTable->getEmptyRecord ();
    
    // now get the predicate
    func predicate = inputRec->compileComputation (selectionPredicate);
    
    // and get the final set of computatoins that will be used to build the output record
    vector <func> finalComputations;
    for (string s : projections) {
        finalComputations.push_back (inputRec->compileComputation (s));
    }
    
    // get the output record
    MyDB_RecordPtr outputRec = outputTable->getEmptyRecord ();
    
    // now, iterate through the input table
    MyDB_RecordIteratorAltPtr myIter = inputTable->getIteratorAlt();
    while (myIter->advance ()) {
        
        // get the current record
        myIter->getCurrent (inputRec);
        
        // see if it is accepted by the predicate
        if (predicate ()->toBool ()) {
            
            // run all of the computations
            int i = 0;
            for (auto f : finalComputations) {
                outputRec->getAtt(i++)->set(f());
            }
            
            // write to output table
            outputRec->recordContentHasChanged ();
            outputTable->append (outputRec);
        }
    }
    
}

#endif
