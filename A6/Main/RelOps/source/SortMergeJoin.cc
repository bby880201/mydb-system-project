//
//  SortMergeJoin.cc
//  A6
//
//  Created by Boyang Bai on 4/16/16.
//  Copyright © 2016 bb26. All rights reserved.
//

#include "SortMergeJoin.h"
#include "Sorting.h"
#include "IteratorComparator.h"
#include <cassert>
#include <fstream>

using namespace std;

SortMergeJoin :: SortMergeJoin (MyDB_TableReaderWriterPtr leftInput,
                                MyDB_TableReaderWriterPtr rightInput,
                                MyDB_TableReaderWriterPtr output,
                                string finalSelectionPredicate,
                                vector <string> projections,
                                pair <string, string> equalityCheck,
                                string leftSelectionPredicate,
                                string rightSelectionPredicate) {
    lInTable = leftInput;
    rInTable = rightInput;
    outTable = output;
    finalPred = finalSelectionPredicate;
    projection = projections;
    equalCheck = equalityCheck;
    lPred = leftSelectionPredicate;
    rPred = rightSelectionPredicate;
}

void SortMergeJoin :: run() {
    auto lKey = equalCheck.first;
    auto rKey = equalCheck.second;
    auto outputRec = outTable->getEmptyRecord ();


    auto lRec1 = lInTable->getEmptyRecord();
    auto lRec2 = lInTable->getEmptyRecord();
    auto lCp1 = lInTable->getEmptyRecord();
    auto lCp2 = lInTable->getEmptyRecord();
    auto lComp = buildRecordComparator (lCp1, lCp2, lKey);
    auto lRuns = selectAndSort(10, *lInTable, lComp, lCp1, lCp2, lPred);
    
    auto rRec1 = rInTable->getEmptyRecord();
    auto rRec2 = rInTable->getEmptyRecord();
    auto rCp1 = rInTable->getEmptyRecord();
    auto rCp2 = rInTable->getEmptyRecord();
    auto rComp = buildRecordComparator (rCp1, rCp2, rKey);
    auto rRuns = selectAndSort(10, *rInTable, rComp, rCp1, rCp2, rPred);
    

    MyDB_SchemaPtr mySchemaOut = make_shared <MyDB_Schema> ();
    for (auto &p : lRec1->getSchema ()->getAtts ())
        mySchemaOut->appendAtt (p);
    for (auto &p : rRec1->getSchema ()->getAtts ())
        mySchemaOut->appendAtt (p);
    
    MyDB_RecordPtr combinedRec = make_shared <MyDB_Record> (mySchemaOut);
    combinedRec->buildFrom (lRec1, rRec1);
    
    func finalPredicate = combinedRec->compileComputation (finalPred);
    func compareLRRecs = combinedRec->compileComputation ("< (" + lKey + ", " + rKey + ")");
    func isEqualLRRecs = combinedRec->compileComputation ("== (" + lKey + ", " + rKey + ")");
    vector <func> finalComputations;
    for (string s : projection) {
        finalComputations.push_back (combinedRec->compileComputation (s));
    }
    
    IteratorComparator lIterComp (lComp, lCp1, lCp2);
    priority_queue <MyDB_RecordIteratorAltPtr, vector <MyDB_RecordIteratorAltPtr>, IteratorComparator> lPq (lIterComp);
    
    // load up the set
    for (MyDB_RecordIteratorAltPtr run : lRuns) {
        if (run->advance ()) {
            lPq.push (run);
        }
    }
    
    IteratorComparator rIterComp (rComp, rCp1, rCp2);
    priority_queue <MyDB_RecordIteratorAltPtr, vector <MyDB_RecordIteratorAltPtr>, IteratorComparator> rPq (rIterComp);
    
    // load up the set
    for (MyDB_RecordIteratorAltPtr run : rRuns) {
        if (run->advance ()) {
            rPq.push (run);
        }
    }
    
    MyDB_RecordPtr combinedRecForTie = make_shared <MyDB_Record> (mySchemaOut);
    combinedRecForTie->buildFrom (lRec1, rRec2);
    func tiedPred = combinedRecForTie->compileComputation (finalPred);
    vector <func> projectionForTie;
    for (string s : projection) {
        projectionForTie.push_back (combinedRecForTie->compileComputation (s));
    }

    vector<MyDB_PageReaderWriter> tiedRecs;
    MyDB_RecordIteratorAltPtr lIter,rIter;
    
    if (lPq.size() != 0 && rPq.size() != 0) {
        lIter = lPq.top();
        lIter->getCurrent(lRec1);
        lPq.pop();
        rIter = rPq.top();
        rIter->getCurrent(rRec1);
        rPq.pop();
    } else {
        return;
    }
//    cout <<"Right Rec: " << rRec1<< "\nLeft Rec: "<< lRec1<< "\n";
    
    int i = 1;
    int j = 1;
//    cout << "queue size: "<< lPq.size()<<"\n" <<rPq.size()<< "\n";

    while (true) {
        
        if (isEqualLRRecs()->toBool()) {
            if (tiedRecs.size() == 0 || !tiedRecs.back().append(rRec1)) {
                tiedRecs.push_back(MyDB_PageReaderWriter(*rInTable->getBufferMgr()));
                tiedRecs.back().append(rRec1);
            }
            
//            flushOutput(outputRec, finalComputations);

            // re-insert
//            cout <<"advance called loc 3" << "\n";
//            cout << rRec1 << "\n" << lRec1<<"\n\n";
            if (rIter->advance ()) {
                rPq.push (rIter);
            }
            if (rPq.size() != 0) {
                rIter = rPq.top();
                rIter->getCurrent(rRec1);
                rPq.pop();
                i++;
            } else {
                while (tiedRecs.size() != 0) {
                    auto tiedIter = getIteratorAlt(tiedRecs);
//                    cout <<"advance called loc 8" << "\n";
                    assert(tiedIter->advance());
                    tiedIter->getCurrent(rRec1);
                    
                    if (finalPredicate()->toBool()) {
                        flushOutput(outputRec, finalComputations);
//                        cout <<"advance called loc 9" << "\n";
                        
                        while (tiedIter->advance()) {
                            tiedIter->getCurrent(rRec1);
                            flushOutput(outputRec, finalComputations);
                        }
                        if (lIter->advance ()) {
                            lPq.push (lIter);
                        }
                        if (lPq.size() != 0) {
                            lIter = lPq.top();
                            lIter->getCurrent(lRec1);
                            lPq.pop();
                        } else {
                            break;
                        }
                    }
                }
                break;
            }
//            cout <<"Right Rec: " << rRec1<< "\nLeft Rec: "<< lRec1<< "\n";

        } else if (compareLRRecs()->toBool()) {
            // left record is smaller than right record
            if (tiedRecs.size() != 0) {
                auto tiedIter = getIteratorAlt(tiedRecs);
//                cout <<"advance called loc 4" << "\n";
                assert(tiedIter->advance());
                tiedIter->getCurrent(rRec2);
                if (tiedPred()->toBool()) {
                    flushOutput(outputRec, projectionForTie);
//                    cout <<"advance called loc 5" << "\n";

                    while (tiedIter->advance()) {
                        tiedIter->getCurrent(rRec2);
                        flushOutput(outputRec, projectionForTie);
                    }
                } else {
                    for (auto page : tiedRecs) {
                        page.clear();
                    }
                    tiedRecs.clear();
                }
            }
            // re-insert
//            cout <<"advance called loc 6" << "\n";
//            cout << rRec1 << "\n" << lRec1<<"\n\n";
            if (lIter->advance ()) {
                lPq.push (lIter);
            }
            if (lPq.size() != 0) {
                lIter = lPq.top();
                lIter->getCurrent(lRec1);
                lPq.pop();
                j++;

                if (isEqualLRRecs()->toBool()) {
                    for (auto page : tiedRecs) {
                        page.clear();
                    }
                    tiedRecs.clear();
                }
            } else {
                break;
            }
//            cout <<"Right Rec: " << rRec1<< "\nLeft Rec: "<< lRec1<< "\n";
            
        } else {
            // re-insert
//            cout <<"advance called loc 7" << "\n";
//            cout << rRec1 << "\n" << lRec1<<"\n\n";
            for (auto page : tiedRecs) {
                page.clear();
            }
            tiedRecs.clear();

            if (rIter->advance ()) {
                rPq.push (rIter);
            }
            if (rPq.size() != 0) {
                rIter = rPq.top();
                rIter->getCurrent(rRec1);
                rPq.pop();
                i++;

            } else {
                break;
            }
//            cout <<"Right Rec: " << rRec1<< "\nLeft Rec: "<< lRec1<< "\n";
        }
    }
//    cout << "i : "<< i << "\nj : " << j << "\n";
   
//    while (tiedRecs.size() != 0) {
//        auto tiedIter = getIteratorAlt(tiedRecs);
//        cout <<"advance called loc 8" << "\n";
//        assert(tiedIter->advance());
//        tiedIter->getCurrent(rRec1);
//        if (tiedPred()->toBool()) {
//            flushOutput(outputRec, projectionForTie);
//            cout <<"advance called loc 9" << "\n";
//            
//            while (tiedIter->advance()) {
//                tiedIter->getCurrent(rRec2);
//                flushOutput(outputRec, projectionForTie);
//            }
//        } else {
//            break;
//        }
//        if (lIter->advance ()) {
//            lPq.push (lIter);
//        }
//        if (lPq.size() != 0) {
//            lIter = lPq.top();
//            lIter->getCurrent(lRec1);
//            lPq.pop();
//        } else {
//            break;
//        }
//    }
}

    
//    cout << "right table's run size: " << lPq.size() <<"\n";
//    while (lPq.size() != 0) {
//        auto myIter = lPq.top();
//        myIter->getCurrent(lRec);
//        cout << lRec<<"\n";
//        lPq.pop ();
//        
//        // re-insert
//        if (myIter->advance ()) {
//            lPq.push (myIter);
//        }
//        
//    }
//    
//    cout << "right table's run size: "<<rPq.size() << "\n";
//    while (rPq.size() != 0) {
//        auto myIter = rPq.top();
//        myIter->getCurrent(rRec);
//        cout << rRec<<"\n";
//        rPq.pop ();
//        
//        // re-insert
//        if (myIter->advance ()) {
//            rPq.push (myIter);
//        }
//        
//    }
    
//    auto lIter = lSortedTable->getIterator(lRec);
//    auto rIter = rSortedTable->getIterator(rRec);
//    
//
//    
//    vector <func> finalComputations;
//    for (string s : projection) {
//        finalComputations.push_back (combinedRec->compileComputation (s));
//    }
//    
//    MyDB_RecordPtr outputRec = outTable->getEmptyRecord ();
//    
//    while (true) {
//        if (finalPredicate ()->toBool ()) {
//            
//            // run all of the computations
//            int i = 0;
//            for (auto f : finalComputations) {
//                outputRec->getAtt (i++)->set (f());
//            }
//            
//            // the record's content has changed because it
//            // is now a composite of two records whose content
//            // has changed via a read... we have to tell it this,
//            // or else the record's internal buffer may cause it
//            // to write old values
//            outputRec->recordContentHasChanged ();
//            output->append (outputRec);
//        }
//
//    }

void SortMergeJoin :: flushOutput(MyDB_RecordPtr outRec,
                                  vector <func> finalComputations) {
    int i = 0;
    for (auto &f : finalComputations) {
        outRec->getAtt (i++)->set (f());
    }
    
    outRec->recordContentHasChanged ();
//    cout << outRec << "\n";
    outTable->append (outRec);
}
