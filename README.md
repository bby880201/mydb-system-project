# README #

Comp530 assignments.

### A1 ###

* LRU Buffer Manager

### A2 ###

* Recode/Page Management

### A3 ###

* Sorted file implementation

### A4 ###

* B+-Tree Implementation

### A5 ###

* Query Optimizer

### A6 ###

* SQL parser and query engine

### A7 ###

* Putting it all together
