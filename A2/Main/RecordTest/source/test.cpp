//
//  test.cpp
//  DB530
//
//  Created by Boyang Bai on 2/13/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#include "MyDB_AttType.h"
#include "MyDB_BufferManager.h"
#include "MyDB_Catalog.h"
#include "MyDB_Page.h"
#include "MyDB_PageReaderWriter.h"
#include "MyDB_Record.h"
#include "MyDB_Table.h"
#include "MyDB_TableReaderWriter.h"
#include "MyDB_Schema.h"
#include "QUnit.h"
#include <iostream>
#include <fstream>

int main () {
    
    QUnit::UnitTest qunit(cerr, QUnit::verbose);
    
    // create the catalog
    {
    }
    
    {
        
        // create a catalog
        MyDB_CatalogPtr myCatalog = make_shared <MyDB_Catalog> ("catFile");
        
        // now make a schema
        MyDB_SchemaPtr mySchema = make_shared <MyDB_Schema> ();
        mySchema->appendAtt (make_pair ("suppkey", make_shared <MyDB_IntAttType> ()));
        mySchema->appendAtt (make_pair ("name", make_shared <MyDB_StringAttType> ()));
        mySchema->appendAtt (make_pair ("address", make_shared <MyDB_StringAttType> ()));
        mySchema->appendAtt (make_pair ("nationkey", make_shared <MyDB_IntAttType> ()));
        mySchema->appendAtt (make_pair ("phone", make_shared <MyDB_StringAttType> ()));
        mySchema->appendAtt (make_pair ("acctbal", make_shared <MyDB_DoubleAttType> ()));
        mySchema->appendAtt (make_pair ("comment", make_shared <MyDB_StringAttType> ()));
        
        // use the schema to create a table
        MyDB_TablePtr myTable = make_shared <MyDB_Table> ("supplier", "supplier.bin", mySchema);
        MyDB_BufferManagerPtr myMgr = make_shared <MyDB_BufferManager> (1024, 16, "tempFile");
        MyDB_TableReaderWriter supplierTable (myTable, myMgr);
        
        // load it from a text file
        supplierTable.loadFromTextFile ("s.tbl");
        
        // put the supplier table into the catalog
        myTable->putInCatalog (myCatalog);
        
        map <string, MyDB_TablePtr> allTables = MyDB_Table :: getAllTables (myCatalog);
        
        // count the records in the 67th page
        MyDB_RecordPtr temp = supplierTable.getEmptyRecord ();
        MyDB_RecordIteratorPtr myIter = supplierTable[1].getIterator (temp);
        while (myIter->hasNext ()) {
            myIter->getNext ();
            cout<<temp<<"\n";
        }
        
        // clear the 67th page
        supplierTable[1].clear ();
        
        // make sure the count is zero now
        int count = 0;
        myIter = supplierTable[1].getIterator (temp);
        while (myIter->hasNext ()) {
            myIter->getNext ();
            count++;
        }
        
        QUNIT_IS_EQUAL (count, 0);
        
    }
    
    {
        {
            
            // load up the table supplier table from the catalog
            MyDB_CatalogPtr myCatalog = make_shared <MyDB_Catalog> ("catFile");
            map <string, MyDB_TablePtr> allTables = MyDB_Table :: getAllTables (myCatalog);
            MyDB_BufferManagerPtr myMgr = make_shared <MyDB_BufferManager> (1024, 16, "tempFile");
            MyDB_TableReaderWriter supplierTable (allTables["supplier"], myMgr);
            
            // test the iterator by looping through all of the records in the file
            MyDB_RecordPtr temp = supplierTable.getEmptyRecord ();
            MyDB_RecordIteratorPtr myIter = supplierTable.getIterator (temp);
            
            // there should be less than 10000 records
            int counter = 0;
            while (myIter->hasNext ()) {
                myIter->getNext ();
                counter++;
                cout<<temp<<"\n";
            }

        }
    }

    
}
