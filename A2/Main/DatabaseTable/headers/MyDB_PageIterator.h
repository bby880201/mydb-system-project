//
//  MyDB_PageIterator.h
//  DB530
//
//  Created by Boyang Bai on 2/12/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#ifndef MyDB_PageIterator_h
#define MyDB_PageIterator_h

#include <memory>
#include "MyDB_RecordIterator.h"
#include "MyDB_Record.h"
#include "MyDB_PageHandle.h"
#include "MyDB_TableIterator.h"

using namespace std;

struct PageInfo {
    size_t pageSize;
    int lastByte;
    char data[0];
};

class MyDB_PageIterator: public MyDB_RecordIterator {
    
public:
    
    // put the contents of the next record in the file/page into the iterator record
    // this should be called BEFORE the iterator record is first examined
    void getNext ();
    
    // return true iff there is another record in the file/page
    bool hasNext ();
    
    // destructor and contructor
    MyDB_PageIterator(MyDB_RecordPtr recordPtr, MyDB_PageHandle pgHandle);
    ~MyDB_PageIterator ();
    
private:
    MyDB_RecordPtr recPtr;
    MyDB_PageHandle pgHdl;
    PageInfo* pgInfo;
//    size_t pgSz;
    
    // below is the change for debugging: added one line
    int lastByte;
    // above is the change for debugging

    size_t passedSz;
//    void* cursor;
//    string pgContent;
//    istringstream *f;
//    string line;
};


#endif /* MyDB_PageIterator_h */
