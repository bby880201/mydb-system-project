//
//  MyDB_TableIterator.h
//  DB530
//
//  Created by Boyang Bai on 2/12/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#ifndef MyDB_TableIterator_h
#define MyDB_TableIterator_h

#include <memory>
#include "MyDB_BufferManager.h"
#include "MyDB_Table.h"
#include "MyDB_RecordIterator.h"
#include "MyDB_Record.h"
#include "MyDB_PageIterator.h"
using namespace std;

class MyDB_TableIterator: public MyDB_RecordIterator {
    
public:
    
    // put the contents of the next record in the file/page into the iterator record
    // this should be called BEFORE the iterator record is first examined
    void getNext ();
    
    // return true iff there is another record in the file/page
    bool hasNext ();
    
    // destructor and contructor
    MyDB_TableIterator(MyDB_RecordPtr recordPtr, MyDB_TablePtr tblPtr, MyDB_BufferManagerPtr bufMgr);
    ~MyDB_TableIterator () {};
    
private:
    MyDB_BufferManagerPtr bufMgrPtr;
    MyDB_TablePtr tablePtr;
    MyDB_RecordPtr toRec;
    MyDB_RecordIteratorPtr pgIter;
    size_t pageSize;
    size_t cursor;
    
};




#endif /* MyDB_TableIterator_h */
