//
//  MyDB_TableIterator.cc
//  DB530
//
//  Created by Boyang Bai on 2/12/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#include <stdio.h>
#include "MyDB_TableIterator.h"

MyDB_TableIterator::MyDB_TableIterator(MyDB_RecordPtr recordPtr, MyDB_TablePtr tblPtr, MyDB_BufferManagerPtr bufMgr):MyDB_RecordIterator(){
    tablePtr =tblPtr;
    toRec = recordPtr;
    bufMgrPtr = bufMgr;
    cursor = 0;
    MyDB_PageHandle pg =bufMgrPtr->getPage(tablePtr, 0);
    pageSize= bufMgrPtr->getPageSize();
    MyDB_RecordIteratorPtr ptr (new MyDB_PageIterator(toRec, pg));
    pgIter = ptr;
}

void MyDB_TableIterator::getNext(){
    pgIter->getNext();
}

bool MyDB_TableIterator::hasNext(){
    if (cursor==tablePtr->lastPage() && !pgIter->hasNext()){
        return false;
    } else {
        
        // below is the change for debugging: edited one line
        // original:
        //if (!pgIter->hasNext()) {
        // changed to:
        while (!pgIter->hasNext()) {
            if (cursor==tablePtr->lastPage()) return false;
        // above is the change for debugging

            cursor++;
            MyDB_PageHandle pg =bufMgrPtr->getPage(tablePtr, cursor);
            MyDB_RecordIteratorPtr ptr (new MyDB_PageIterator(toRec, pg));
            pgIter = ptr;
        }
        return true;
    }
}