
#ifndef TABLE_RW_C
#define TABLE_RW_C

#include <fstream>
#include "MyDB_PageReaderWriter.h"
#include "MyDB_TableReaderWriter.h"

using namespace std;

MyDB_TableReaderWriter :: MyDB_TableReaderWriter (MyDB_TablePtr tblPtr, MyDB_BufferManagerPtr bufPtr) {
    
    tablePtr = tblPtr;
    bufMgrPtr = bufPtr;
    emptyRec = getEmptyRecord();
}

MyDB_PageReaderWriter &MyDB_TableReaderWriter :: operator [] (size_t i) {
//	static MyDB_PageReaderWriter temp;
    if (i>tablePtr->lastPage()) {
        i = tablePtr->lastPage();
        cerr << "page reader/writer out of bound!" <<endl;
    }
    
    MyDB_PageHandle pg = bufMgrPtr->getPage(tablePtr, i);
    MyDB_PageReaderWriter temp = MyDB_PageReaderWriter(pg);
    
    pgReader.push_back(temp);
	return pgReader.back();
}

MyDB_RecordPtr MyDB_TableReaderWriter :: getEmptyRecord () {
    MyDB_RecordPtr ptr (new MyDB_Record(tablePtr->getSchema()));
    return ptr;
}

MyDB_PageReaderWriter &MyDB_TableReaderWriter :: last () {
	return (*this)[tablePtr->lastPage()];
}


void MyDB_TableReaderWriter :: append (MyDB_RecordPtr appendMe) {
    MyDB_PageReaderWriter lastPage = this->last();
    if (!lastPage.append(appendMe)) {
        int last =tablePtr->lastPage()+1;
        MyDB_PageHandle pg = bufMgrPtr->getPage(tablePtr, last);
        tablePtr->setLastPage(last);
        
        // below is the change for debugging: added one line
        (*this)[last].clear();
        // above is the change for debugging
       
        (*this)[last].append(appendMe);
    }
}

void MyDB_TableReaderWriter :: loadFromTextFile (string fileName) {
    long i = 0;

    ifstream myReadFile;
    myReadFile.open(fileName);
    string s;
    MyDB_PageHandle pg = bufMgrPtr->getPage(tablePtr, i);
    PageInfo* temp =getPageInfo(pg);
    MyDB_RecordPtr tempRec = getEmptyRecord();
    pg->wroteBytes();
    
    while (getline(myReadFile,s)){
        tempRec->fromString(s);
        ssize_t sz = tempRec->getBinarySize();
        if (temp->lastByte + sz + sizeof(PageInfo) > temp->pageSize){
            i++;
            pg = bufMgrPtr->getPage(tablePtr, i);
            temp = getPageInfo(pg);
            pg->wroteBytes();
        }
        tempRec->toBinary(temp->data+temp->lastByte);
        temp->lastByte += sz;
    }
    myReadFile.close();
    tablePtr->setLastPage(i);
}

PageInfo* MyDB_TableReaderWriter::getPageInfo(MyDB_PageHandle pg){
    
    struct PageInfo *temp = (PageInfo *) (pg->getBytes ());
    temp->pageSize = bufMgrPtr->getPageSize();
    temp->lastByte = 0;
    return temp;
}


MyDB_RecordIteratorPtr MyDB_TableReaderWriter :: getIterator (MyDB_RecordPtr toRec) {
     MyDB_RecordIteratorPtr ptr (new MyDB_TableIterator(toRec, tablePtr, bufMgrPtr));
	return ptr;
}

void MyDB_TableReaderWriter :: writeIntoTextFile (string fileName) {
    
    ofstream myWtriteFile;
    myWtriteFile.open(fileName);
    
    for (int i=0; i<=tablePtr->lastPage();i++){
        MyDB_RecordPtr rec = this->getEmptyRecord();
        MyDB_RecordIteratorPtr myIter = (*this)[i].getIterator(rec);
        while (myIter->hasNext ()) {
            myIter->getNext ();
            myWtriteFile<<rec<<"\n";
        }
    }

    myWtriteFile.close();
}

MyDB_TableReaderWriter::~MyDB_TableReaderWriter(){
    
};


#endif

