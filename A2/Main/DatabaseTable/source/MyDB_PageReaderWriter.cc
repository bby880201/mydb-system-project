
#ifndef PAGE_RW_C
#define PAGE_RW_C

#include "MyDB_PageReaderWriter.h"

void MyDB_PageReaderWriter :: clear () {
    pgInfo->lastByte = 0;
    page->wroteBytes();
}

MyDB_PageType MyDB_PageReaderWriter :: getType () {
	return MyDB_PageType :: RegularPage;
}

MyDB_RecordIteratorPtr MyDB_PageReaderWriter :: getIterator (MyDB_RecordPtr recPtr) {
    MyDB_RecordIteratorPtr ptr (new MyDB_PageIterator(recPtr, page));
    return ptr;
}

void MyDB_PageReaderWriter :: setType (MyDB_PageType pgType) {
    pageType = pgType;
}

bool MyDB_PageReaderWriter :: append (MyDB_RecordPtr preRecord) {
    if (preRecord->getBinarySize() + pgInfo->lastByte + sizeof(PageInfo)<pageSz){
        preRecord->toBinary(pgInfo->data+pgInfo->lastByte);
        pgInfo->lastByte+=preRecord->getBinarySize();
        return true;
    }else{
        return false;
    }
}

MyDB_PageReaderWriter::MyDB_PageReaderWriter(MyDB_PageHandle pgHandle){
    page = pgHandle;
    pgInfo = (PageInfo*) page->getBytes();
    pageSz = pgInfo->pageSize;
}

#endif
