//
//  MyDB_PageIterator.cc
//  DB530
//
//  Created by Boyang Bai on 2/12/16.
//  Copyright © 2016 kq2. All rights reserved.
//

#include <stdio.h>
#include <sstream>
#include <iostream>
#include "MyDB_PageIterator.h"

MyDB_PageIterator::MyDB_PageIterator(MyDB_RecordPtr recordPtr, MyDB_PageHandle pgHandle):MyDB_RecordIterator(){
    recPtr = recordPtr;
    pgHdl = pgHandle;
    pgInfo = (PageInfo*)pgHdl->getBytes();
    
    // below is the change for debugging: added one line
    lastByte = pgInfo->lastByte;
    // above is the change for debugging
    
//    pgSz = size;
    passedSz = 0;
////    cursor = pgHdl->getBytes();
//    char* buf = (char*) pgHdl->getBytes();
//    pgContent.assign(buf,pgSz);
//    
//    f = new istringstream(pgContent);
}

void MyDB_PageIterator::getNext(){
    recPtr->fromBinary(pgInfo->data+passedSz);
    passedSz += recPtr->getBinarySize();
    
//    try {
//        recPtr->fromString(line);
//    } catch (std::invalid_argument) {
//    }
}

bool MyDB_PageIterator::hasNext(){

//    if (getline(*f, line) && passedSz+line.size() < pgSz  ){
//        passedSz += line.size();
//        return true;
//    }else {
//        return false;
//    }
    
    // below is the change for debugging: edited one line
    // original:
    //if (passedSz>=pgInfo->lastByte) {
    // changed to:
    if (passedSz>=lastByte) {
     // above is the change for debugging
        
        return false;
    }else{
        return true;
    }
    
}

MyDB_PageIterator::~MyDB_PageIterator () {
}